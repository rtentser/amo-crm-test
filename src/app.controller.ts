import { Controller, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async createDeal(@Query() query): Promise<void> {
    await this.appService.createDeal(
      query['name'],
      query['mail'],
      query['phone'],
    );
  }
}
