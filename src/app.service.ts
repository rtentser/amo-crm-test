import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AxiosError } from 'axios';
import { catchError, firstValueFrom, map } from 'rxjs';
import { AuthTokens } from './entities/auth.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AppService {
  private readonly logger = new Logger(AppService.name);

  private readonly _contactEndpoint = '/api/v4/contacts';
  private readonly _leadsEndpoint = '/api/v4/leads';
  private readonly _refreshEndpoint = '/oauth2/access_token';

  constructor(
    @InjectRepository(AuthTokens)
    private readonly authTokensRepository: Repository<AuthTokens>,
    private configService: ConfigService,
    private readonly httpService: HttpService,
  ) {}

  async createDeal(name: string, mail: string, phone: string): Promise<void> {
    await this._refreshTokens();
    const id = await this.getContact(name, mail, phone);
    const deal = { _embedded: { contacts: [{ id: id }] } };

    await firstValueFrom(
      this.httpService
        .post(this._leadsEndpoint, [deal], await this._requestParams())
        .pipe(
          map((response) => {
            return response.data;
          }),
          catchError((error: AxiosError) => {
            this.logger.error(
              error.response.data['validation-errors'][0]['errors'],
            );
            throw 'An error happened!';
          }),
        ),
    );
  }

  async getContact(name: string, mail: string, phone: string): Promise<number> {
    let contactData = await firstValueFrom(
      this.httpService
        .get(
          this._contactEndpoint + `?query=${mail}`,
          await this._requestParams(),
        )
        .pipe(
          map((response) => {
            return response.data;
          }),
          catchError((error: AxiosError) => {
            this.logger.error(
              error.response.data['validation-errors'][0]['errors'],
            );
            throw 'An error happened!';
          }),
        ),
    );

    if (!contactData) {
      contactData = await firstValueFrom(
        this.httpService
          .get(
            this._contactEndpoint + `?query=${phone}`,
            await this._requestParams(),
          )
          .pipe(
            map((response) => {
              return response.data;
            }),
            catchError((error: AxiosError) => {
              this.logger.error(
                error.response.data['validation-errors'][0]['errors'],
              );
              throw 'An error happened!';
            }),
          ),
      );
    }

    if (!contactData) return await this._createContact(name, mail, phone);
    if (contactData) {
      const id = contactData._embedded['contacts'][0]['id'];
      return await this._updateContact(id, name, mail, phone);
    }
  }

  private async _createContact(
    name: string,
    mail: string,
    phone: string,
  ): Promise<number> {
    const contact = {
      name: name,
      custom_fields_values: [
        {
          field_id: 338075,
          values: [{ value: phone }],
        },
        {
          field_id: 338077,
          values: [{ value: mail }],
        },
      ],
    };

    const contactData = await firstValueFrom(
      this.httpService
        .post(this._contactEndpoint, [contact], await this._requestParams())
        .pipe(
          map((response) => {
            return response.data;
          }),
          catchError((error: AxiosError) => {
            this.logger.error(
              error.response.data['validation-errors'][0]['errors'],
            );
            throw 'An error happened!';
          }),
        ),
    );

    return contactData._embedded['contacts'][0]['id'];
  }

  private async _updateContact(
    id: number,
    name: string,
    mail: string,
    phone: string,
  ): Promise<number> {
    const contact = {
      id: id,
      name: name,
      custom_fields_values: [
        {
          field_id: 338075,
          values: [{ value: phone }],
        },
        {
          field_id: 338077,
          values: [{ value: mail }],
        },
      ],
    };

    const contactData = await firstValueFrom(
      this.httpService
        .patch(this._contactEndpoint, [contact], await this._requestParams())
        .pipe(
          map((response) => {
            return response.data;
          }),
          catchError((error: AxiosError) => {
            this.logger.error(
              error.response.data['validation-errors'][0]['errors'],
            );
            throw 'An error happened!';
          }),
        ),
    );

    return contactData._embedded['contacts'][0]['id'];
  }

  private async _requestParams() {
    const accessToken = (
      await this.authTokensRepository.find({
        select: {
          accessToken: true,
        },
      })
    )[0].accessToken;

    return {
      baseURL: this.configService.get('BASE_URL'),
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
  }

  private async _refreshTokens(): Promise<void> {
    const authToken = (
      await this.authTokensRepository.find({
        select: {
          id: true,
          refreshToken: true,
          expiresIn: true,
          updatedAt: true,
        },
      })
    )[0];

    const expirationDate = authToken.updatedAt.setSeconds(
      authToken.updatedAt.getSeconds() + authToken.expiresIn,
    );

    console.log(authToken);

    if (expirationDate < Date.now()) {
      const refreshmentData = {
        client_id: this.configService.get('CLIENT_ID'),
        client_secret: this.configService.get('CLIENT_SECRET'),
        grant_type: 'refresh_token',
        refresh_token: authToken.refreshToken,
        redirect_uri: this.configService.get('REDIRECT_URI'),
      };

      const authData = await firstValueFrom(
        this.httpService
          .post(this._refreshEndpoint, refreshmentData, {
            baseURL: this.configService.get('BASE_URL'),
          })
          .pipe(
            map((response) => {
              return response.data;
            }),
            catchError((error: AxiosError) => {
              this.logger.error(error.response.data);
              throw 'An error happened!';
            }),
          ),
      );

      await this.authTokensRepository.save({
        id: authToken.id,
        expiresIn: authData.expires_in,
        accessToken: authData.access_token,
        refreshToken: authData.refresh_token,
      });
    }
  }
}
